const contractConfig = require('../contract-config.js');
const Energi = artifacts.require('Energi');

module.exports = (deployer, network, accounts) => {
  deployer.then(async () => {
    const adminAddress = accounts[0] || contractConfig[network].adminAddress;
    const energi = await deployer.deploy(Energi, { from: adminAddress });
    process.env.energiAddress = energi.address;
  })
}
