// SPDX-License-Identifier: MIT
pragma solidity ^0.5.13;

contract Energi {
  uint256 lastBlock;
  address lastAddress;
  constructor() public {}

  function receiveEther() external payable {
    require(msg.value > 0, 'Must send more than Zero!');
    if (block.number - lastBlock > 100 && lastBlock != 0 && lassAddress != address(0)) {
      lastAddress.transfer(msg.value);
    }
    lastBlock = block.number;
    lastAddress = msg.sender;
  }
}
